import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import { NavController, ToastController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(
//    private nav: NavController,
    private auth: AngularFireAuth,
    private toast: ToastController,

  ) { }

  login(user){
    this.auth.signInWithEmailAndPassword(user.email, user.password).
//    then(() => this.nav.navigateForward('home')).
    catch (() => this.showError());
   // console.log(this.email);
   // console.log(this.password);
  }
  private async showError(){
    const ctrl = await this.toast.create({
      message: 'Dados de acesso incorretos',
      duration: 3000
    });

    ctrl.present();

  }

  createUser(user){
    this.auth.createUserWithEmailAndPassword(user.email, user.password).
    then(credentials => console.log(credentials));
  }

  recoverPass(email){

  }
}
